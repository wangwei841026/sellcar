var Category = require('../model/category');

module.exports.getallcategories = function(req, res) {

    Category
        .find()
        .exec(function(err, categories) {
            res.status(200).json(categories);

        });

};
module.exports.getcategories = function(req, res) {

    Category
        .find({ parent: null })
        .exec(function(err, categories) {
            res.status(200).json(categories);

        });

};
module.exports.addcategory = function(req, res) {

    var category = new Category();
    category.name = req.body.name;
    if (req.body.parent !== "") {
        category.parent = req.body.parent;
    } else {
        category.parent = null;
    }
    if (req.body.type !== "") {
        category.type = req.body.type;
    } else {
        category.type = null;
    }
    category.save(function(err) {
        if (err) {
            res.status(404).json(err);
            return;
        }
        res.status(200).json('success');
    });
};
module.exports.updatecategory = function(req, res, next) {

    Category.findOne({ _id: req.body.id }, function(err, category) {
        category.name = req.body.name;

        if (req.body.parent !== "") {
            category.parent = req.body.parent;
        } else {
            category.parent = null;
        }

        if (req.body.type !== "") {
            category.type = req.body.type;
        } else {
            category.type = null;
        }
        category.save(function(err) {
            if (err) {
                res.status(404).json(err);
                return;
            }
            res.status(200).json('success');
        });

    });
};
module.exports.deletecategory = function(req, res) {
    Category
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};