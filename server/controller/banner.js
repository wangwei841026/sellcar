var Banner = require('../model/banner');

module.exports.getbanners = function(req, res) {

    Banner
        .find()
        .exec(function(err, banners) {
            res.status(200).json(banners);

        });

};

module.exports.addbanner = function(req, res) {
    console.log(req.body);
    var banner = new Banner();
    banner.title = req.body.title;
    banner.target = req.body.target;
    banner.image_url = req.body.image_url;
    banner.startDate = new Date(req.body.startDate);
    banner.endDate = new Date(req.body.endDate);
    banner.save(function(err) {
        if (err) {
            res.status(404).json(err);
            return;
        }
        res.status(200).json('success');
    });
};
module.exports.updatebanner = function(req, res, next) {

    Banner.findOne({ _id: req.body.id }, function(err, banner) {
        banner.title = req.body.title;
        banner.target = req.body.target;
        banner.image_url = req.body.image_url;
        banner.startDate = new Date(req.body.startDate);
        banner.endDate = new Date(req.body.endDate);

        banner.save(function(err) {
            if (err) {
                res.status(404).json(err);
                return;
            }
            res.status(200).json('success');
        });

    });
};
module.exports.deletebanner = function(req, res) {
    Banner
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};