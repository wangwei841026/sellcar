var Transaction = require('../model/transaction');

module.exports.gettransactions = function(req, res) {

    Transaction
        .find()
        .exec(function(err, vehicles) {
            res.status(200).json(vehicles);

        });

};

module.exports.deletetransaction = function(req, res) {
    Transaction
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};
module.exports.transactionbyvehicle = function(req, res) {

    Transaction.aggregate([
            { "$match": { "status": { "$gt": 5 } } },
            {
                "$group": {
                    "_id": "$vehicle",
                    "totalPrice": {
                        "$sum": "$finalPrice"
                    }
                },
            },
            {
                "$sort": {
                    "totalPrice": -1,

                }
            },
            {
                "$lookup": {
                    "from": "vehicles",
                    "localField": '_id',
                    "foreignField": '_id',
                    "as": 'vehicle',
                }
            },
            // {
            //     "$lookup": {
            //         "from": "categories",
            //         "localField": 'vehicle.model',
            //         "foreignField": '_id',
            //         "as": 'vehicle.model'
            //     }
            // },
            // {
            //     "$lookup": {
            //         "from": "categories",
            //         "localField": 'vehicle.brand',
            //         "foreignField": '_id',
            //         "as": 'vehicle.brand'
            //     }
            // },


        ])
        .exec(function(err, result) {
            res.status(200).json(result);
        });
};
module.exports.transactionbybuyer = function(req, res) {

    Transaction.aggregate([
            { "$match": { "status": { "$gt": 5 } } },
            {
                "$group": {
                    "_id": "$buyer",
                    "totalPrice": {
                        "$sum": "$finalPrice"
                    }
                },
            },
            {
                "$sort": {
                    "totalPrice": -1,

                }
            },

        ])
        .exec(function(err, result) {
            res.status(200).json(result);
        });
};