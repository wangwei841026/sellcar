var User = require('../model/user');

module.exports.addadmin = function(req, res) {
    console.log(req.body);
    var user = new User();
    user.email = req.body.email;
    user.firstName = req.body.firstname;
    user.lastName = req.body.lastname;
    user.role = "admin";
    user.setPassword(req.body.password);
    user.save(function(err) {
        if (err) {
            res.status(404).json(err);
            return;
        }
        res.status(200).json('success');
    });
};
module.exports.getalladmins = function(req, res) {

    User
        .find({ role: "admin" })
        .exec(function(err, users) {
            res.status(200).json(users);

        });

};
module.exports.deleteadmin = function(req, res) {
    User
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};
module.exports.getallbuyers = function(req, res) {

    User
        .find({ role: "buyer" })
        .exec(function(err, users) {
            res.status(200).json(users);

        });

};
module.exports.deletebuyer = function(req, res) {
    User
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};
module.exports.getallsellers = function(req, res) {

    User
        .find({ role: "seller" })
        .exec(function(err, users) {
            res.status(200).json(users);

        });

};
module.exports.deleteseller = function(req, res) {
    User
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};
module.exports.getuser = function(req, res) {
    User
        .find({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json(user);
        });
};