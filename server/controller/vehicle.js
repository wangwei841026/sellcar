var Vehicle = require('../model/vehicle');

module.exports.getvehicles = function(req, res) {

    Vehicle
        .find()
        .exec(function(err, vehicles) {
            res.status(200).json(vehicles);

        });

};

module.exports.deletevehicle = function(req, res) {
    Vehicle
        .findOneAndDelete({ _id: req.params.id })
        .exec(function(err, user) {
            if (err) res.json(err);
            res.status(200).json('success');
        });
};