var passport = require('passport');
var User = require('../model/user');


//login
module.exports.login = function(req, res) {

    passport.authenticate('local', function(err, user, info) {
        var token;
        // If Passport throws/catches an error
        if (err) {
            res.status(404).json(err);
            return;
        }
        // If a user is found
        if (user) {
            if (user.role != "admin") {
                res.json({
                    "success": false,
                    "message": "You are not permitted."
                });
                return;
            }
            token = user.generateJwt();
            res.status(200);
            res.json({
                "success": true,
                "token": token
            });
        } else {
            // If user is not found
            res.status(401).json({
                "success": false,
                "message": "User not found"
            });
        }
    })(req, res);
};

// add user
module.exports.adduser = function(req, res) {
    console.log(req.body);
    var user = new User();
    user.email = req.body.email;
    user.setPassword(req.body.password);
    user.save(function(err) {
        if (err) {
            res.status(404).json(err);
            return;
        }
        res.status(200).json('user');
    });
};