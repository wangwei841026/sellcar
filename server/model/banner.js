var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var banner = new Schema({
    title: {
        type: String,
        required: true
    },
    target: {
        type: String,
    },
    image_url: {
        type: String,

    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    }
}, {
    collection: 'banners'
});



module.exports = mongoose.model('banner', banner);