var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;
var vehicle = new Schema({
    status: {
        type: String,
    },
    certification: {
        year: String,
        color: String,
        transmittion: String,
        distance: Number,
        taxDate: Date,
        stnkExpiredDate: Date,
        engine: Number,
        fuel: String
    },
    location: {
        tyhpe: String,
        coordinates: {
            type: Array,
        }
    },
    tags: {
        type: Array,
    },
    periodPrice: {
        type: Array
    },
    photos: {
        type: Array,
    },
    isAutoNegotiation: {
        type: Boolean
    },
    bottomPrice: {
        type: Number
    },
    salePrice: {
        type: Number
    },
    tipe: {
        type: String
    },
    year: {
        type: String
    },
    transmittion: {
        type: String
    },
    machine: {
        type: String
    },
    kilometer: {
        type: Number
    },
    passenger: {
        type: Number
    },
    color: {
        type: String
    },
    description: {
        type: String
    },
    brand: {
        type: ObjectId
    },
    varian: {
        type: ObjectId
    },
    model: {
        type: ObjectId
    },
    title: {
        type: String
    },
    user: {
        type: ObjectId
    },
    viewCount: {
        type: Number
    }

}, {
    collection: 'vehicles'
});



module.exports = mongoose.model('vehicle', vehicle);