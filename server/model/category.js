var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;
var category = new Schema({
    name: {
        type: String,
        required: true
    },
    parent: {
        type: ObjectId,
    },
    type: {
        type: String,

    },
}, {
    collection: 'categories'
});



module.exports = mongoose.model('category', category);