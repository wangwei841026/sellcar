var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;
var Carseller_SECRET = 'hl6naLlJIu97bq60_94LHAoogHSXMSBEb9MjnV-lulbICNBAnvYSYMbzNF99qR6z-aKYxtn85E11JYfr-fcqu5svA41pHbUho323KTXirVGNyzCTh9TLiqcZku4nsOCsKcGeMnRi5lVdN-EgozOYSHPAmCWqcBMw2DQcYYfsPoEYmzmH9lPx638T-hOJY4oaZIaTMKgw1HEPVrztIyPwZOExCyqR7nvN_htOGx8VeDbs8V9fu9OT-iwzDWyzeEIn3aWC7wb-bgKuBzTcjPWNB-Y6CvI-NXX_b8xGPwmsqec9I1CvZ6EfI3F51fngG4n-utNM6E7Mzy_9KfkYPTfBAQ'

var user = new Schema({
    email: {
        type: String,
        required: true
    },
    handphone: {
        type: String
    },
    ktpId: {
        type: String,

    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    survey: {
        type: Array
    },
    photo: {
        type: String
    },
    status: {
        type: Number,

    },
    handphoneVerified: {
        type: Boolean
    },
    otp: {
        type: String,

    },
    settings: {
        pushNotification: Boolean,
        language: String
    },

    hash: String,
    salt: String
}, {
    collection: 'users'
});

user.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

user.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

user.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    return jwt.sign({
        _id: this._id,
        email: this.email,
        role: this.role,
        exp: parseInt(expiry.getTime() / 1000),
    }, Carseller_SECRET);
};

module.exports = mongoose.model('user', user);