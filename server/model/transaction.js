var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;
var transaction = new Schema({
    vehicle: {
        type: ObjectId,
    },
    buyer: {
        type: ObjectId,
    },
    finalPrice: {
        type: Number,

    },
    status: {
        type: Number
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    }

}, {
    collection: 'transactions'
});



module.exports = mongoose.model('transaction', transaction);