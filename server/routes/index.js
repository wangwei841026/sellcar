var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');

var auth = jwt({
    secret: 'hl6naLlJIu97bq60_94LHAoogHSXMSBEb9MjnV-lulbICNBAnvYSYMbzNF99qR6z-aKYxtn85E11JYfr-fcqu5svA41pHbUho323KTXirVGNyzCTh9TLiqcZku4nsOCsKcGeMnRi5lVdN-EgozOYSHPAmCWqcBMw2DQcYYfsPoEYmzmH9lPx638T-hOJY4oaZIaTMKgw1HEPVrztIyPwZOExCyqR7nvN_htOGx8VeDbs8V9fu9OT-iwzDWyzeEIn3aWC7wb-bgKuBzTcjPWNB-Y6CvI-NXX_b8xGPwmsqec9I1CvZ6EfI3F51fngG4n-utNM6E7Mzy_9KfkYPTfBAQ',
    userProperty: 'payload'
});

//define every controllers
var ctrlAuth = require('../controller/authentication');
var ctrlUser = require('../controller/user');
var ctrlCategory = require('../controller/category');
var ctrlBanner = require('../controller/banner');
var ctrlVehicle = require('../controller/vehicle');
var ctrlTransaction = require('../controller/transaction');
// define routes

// router.post('/register', ctrlAuth.adduser);
router.post('/login', ctrlAuth.login);
router.post('/addadmin', ctrlUser.addadmin);
router.get('/getalladmins', ctrlUser.getalladmins);
router.get('/deleteadmin/:id', ctrlUser.deleteadmin);
router.get('/getallbuyers', ctrlUser.getallbuyers);
router.get('/deletebuyer/:id', ctrlUser.deletebuyer);
router.get('/getallsellers', ctrlUser.getallsellers);
router.get('/deleteseller/:id', ctrlUser.deleteseller);
router.get('/getallcategories', ctrlCategory.getallcategories);
router.get('/getcategories', ctrlCategory.getcategories);
router.post('/addcategory', ctrlCategory.addcategory);
router.post('/updatecategory', ctrlCategory.updatecategory);
router.get('/deletecategory/:id', ctrlCategory.deletecategory);
router.get('/getbanners', ctrlBanner.getbanners);
router.post('/addbanner', ctrlBanner.addbanner);
router.post('/updatebanner', ctrlBanner.updatebanner);
router.get('/deletebanner/:id', ctrlBanner.deletebanner);
router.get('/getvehicles', ctrlVehicle.getvehicles);
router.get('/deletevehicle/:id', ctrlVehicle.deletevehicle);
router.get('/gettransactions', ctrlTransaction.gettransactions);
router.get('/deletetransaction/:id', ctrlTransaction.deletetransaction);
router.get('/transactionbyvehicle', ctrlTransaction.transactionbyvehicle);
router.get('/transactionbybuyer', ctrlTransaction.transactionbybuyer);

module.exports = router;