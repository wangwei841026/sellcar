import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import App from './container/App';
import "react-datepicker/dist/react-datepicker.css";
import 'mdbreact/dist/css/mdb.css';
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import './index.css';
const store = configureStore();

ReactDOM.render(
  <BrowserRouter >
    <Provider store={store}>
      <App/>
    </Provider>
  </BrowserRouter>
  , document.getElementById('root'));