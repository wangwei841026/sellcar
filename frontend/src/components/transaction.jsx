import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';

export default class Transaction extends Component {
     constructor(props) {
          super(props);
          this.state = {
              transactions: [],
              vehicles:[],
              buyers:[],
              tablelist:[],
              deletetransaction:'',
              alertmodal:false,
          };
         
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);      
     }

     componentWillMount(){
          var self=this;
          var result = [];
          axios.get('http://localhost:4000/api/getvehicles')
               .then(response => {
                    this.setState({vehicles:response.data});
                    axios.get('http://localhost:4000/api/getallbuyers')
                    .then (response=>{
                         this.setState({buyers:response.data});
                         axios.get('http://localhost:4000/api/gettransactions')
                         .then(response => {
                              this.setState({transactions:response.data});
                              response.data.map(function(transaction, i){
                                   return result.push({"no":(i+1), "vehicle":transaction.vehicle, "buyer":self.getusername(transaction.buyer),"finalPrice":transaction.finalPrice,
                                   "status":transaction.status, "createdDate":self.timeConverter(transaction.createdDate), "updatedDate":self.timeConverter(transaction.updatedDate), 
                                   "remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deletetransaction(e, transaction._id)}}>Remove</MDBBtn>});
                                                       
                              });
                              this.setState({tablelist: result});
                              
                         })
                    })
                         
                    
               })
               
     }
     
     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 

    
      deletetransaction(event, id) {
          this.setState({deletetransaction: id});
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deletetransaction/'+this.state.deletetransaction)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              
      }
      timeConverter(UNIX_timestamp) {
          var a = new Date(UNIX_timestamp);
          var year = a.getFullYear();
          var month = a.getMonth() + 1;
          month = month.toString().length === 1 ? "0" + month : month;
          var date = a.getDate();
          date = date.toString().length === 1 ? "0" + date : date;
          var time = month + '/' + date + '/' + year;
  
          return time;
      }
      
     getusername(id) {
          console.log(this.state.buyers);
          if (id!==null) {
               var buyer=this.state.buyers.find((e)=>e._id===id);
               
               if (buyer!==undefined) {
                    return buyer['firstName'];
               }       
          } else {
               return "";
          }
     }
      
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 100
                 },
                 {
                   label: 'vehicle',
                   field: 'vehicle',
                   width: 350
                 },
                 {
                    label: 'buyer',
                    field: 'buyer',
                    width: 250
                  },
                 {
                    label: 'finalPrice',
                    field: 'finalPrice',
                    width: 250
                  },
                 {
                   label: 'status',
                   field: 'status',
                   width: 250
                 },
                 {
                    label: 'createdDate',
                    field: 'createdDate',
                    width: 100
                  },
                  {
                    label: 'updatedDate',
                    field: 'updatedDate',
                    width: 200
                  },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 200
                 }
               ],
               rows: this.state.tablelist
          };
         
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Transaction</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />

                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }