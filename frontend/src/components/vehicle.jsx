import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';

export default class Vehicle extends Component {
     constructor(props) {
          super(props);
          this.state = {
              vehicles: [],
              categories:[],
              sellers:[],
              tablelist:[],
              selectedvehicle:[],
              deletevehicle:'',
              modalIsOpen: false,
              alertmodal:false,
          };
          this.openModal = this.openModal.bind(this);
          this.closeModal = this.closeModal.bind(this);
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);      
     }

     componentWillMount(){
          var self=this;
          var result = [];
          axios.get('http://localhost:4000/api/getallcategories')
               .then(response => {
                    this.setState({categories:response.data});
                    axios.get('http://localhost:4000/api/getallsellers')
                    .then (response=>{
                         this.setState({sellers:response.data});
                         axios.get('http://localhost:4000/api/getvehicles')
                         .then(response => {
                              this.setState({vehicles:response.data});
                              response.data.map(function(vehicle, i){
                                   return result.push({"no":(i+1), "title":vehicle.title, "status":vehicle.status,"bottomPrice":vehicle.bottomPrice, "salePrice":vehicle.salePrice,
                                   "tipe":vehicle.tipe, "year":vehicle.year, "transmittion":vehicle.transmittion, "machine":vehicle.machine,
                                   "viewdetails":<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.showvehicle(e, vehicle._id)}}>View Details</MDBBtn>,
                                   "remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deletevehicle(e, vehicle._id)}}>Remove</MDBBtn>});
                                                       
                              });
                              this.setState({tablelist: result});
                              
                         })
                    })
                         
                    
               })
               
     }
     openModal() {
          this.setState({modalIsOpen: true});
     }
       
     closeModal() {
          this.setState({modalIsOpen: false});
     }

     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 

     showvehicle(event,id) {
          this.setState({modalIsOpen: true});
          var selectedvehicle=this.state.vehicles.find((e) => e._id === id);
          this.setState({selectedvehicle:selectedvehicle});
          console.log(this.state.selectedvehicle);
      }
      deletevehicle(event, id) {
          this.setState({deletevehicle: id});
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deletevehicle/'+this.state.deletevehicle)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              
      }
      timeConverter(UNIX_timestamp) {
          var a = new Date(UNIX_timestamp);
          var year = a.getFullYear();
          var month = a.getMonth() + 1;
          month = month.toString().length === 1 ? "0" + month : month;
          var date = a.getDate();
          date = date.toString().length === 1 ? "0" + date : date;
          var time = month + '/' + date + '/' + year;
  
          return time;
      }
      namebyid(id) {
          
          if (id!==null) {
               var category=this.state.categories.find((e)=>e._id===id);
               
               if (category!==undefined) {
                    return category['name'];
               }       
          } else {
               return "";
          }
     }
     getusername(id) {
          console.log(this.state.sellers);
          if (id!==null) {
               var seller=this.state.sellers.find((e)=>e._id===id);
               
               if (seller!==undefined) {
                    return seller['firstName'];
               }       
          } else {
               return "";
          }
     }
      
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 100
                 },
                 {
                   label: 'title',
                   field: 'title',
                   width: 350
                 },
                 {
                    label: 'status',
                    field: 'status',
                    width: 250
                  },
                 {
                    label: 'bottomPrice',
                    field: 'bottomPrice',
                    width: 250
                  },
                 {
                   label: 'salePrice',
                   field: 'salePrice',
                   width: 250
                 },
                 {
                    label: 'tipe',
                    field: 'tipe',
                    width: 100
                  },
                  {
                    label: 'year',
                    field: 'year',
                    width: 200
                  },
                  {
                    label: 'transmittion',
                    field: 'transmittion',
                    width: 250
                  },
                  {
                    label: 'machine',
                    field: 'machine',
                    width: 250
                  },
                  {
                    label: 'View Deails',
                    field: 'viewdetails',
                    width: 200
                  },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 200
                 }
               ],
               rows: this.state.tablelist
          };
         
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Vehicle</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />

                    </div>
                    
                    <div>
                         <Modal show={this.state.modalIsOpen} onHide={this.closeModal} size="lg">
                              <Modal.Header closeButton>
                                   <Modal.Title>View Details</Modal.Title>
                              </Modal.Header>
                              <Modal.Body className="vehiclemodal">
                                   <p>status: {this.state.selectedvehicle.status}</p>
                                   <p>certification: </p>
                                        <p style={{marginLeft:20}}>year: {this.state.selectedvehicle.certification?this.state.selectedvehicle.certification['year']:""}</p>
                                        <p style={{marginLeft:20}}>color: {this.state.selectedvehicle.certification?this.state.selectedvehicle.certification['color']:""}</p>
                                        <p style={{marginLeft:20}}>trasnmittion: {this.state.selectedvehicle.certification?this.state.selectedvehicle.certification['trasnmittion']:""}</p>
                                        <p style={{marginLeft:20}}>taxDate: {this.state.selectedvehicle.certification?this.timeConverter(this.state.selectedvehicle.certification['taxDate']):""}</p>
                                        <p style={{marginLeft:20}}>stnkExpiredDate: {this.state.selectedvehicle.certification?this.timeConverter(this.state.selectedvehicle.certification['stnkExpiredDate']):""}</p>
                                        <p style={{marginLeft:20}}>engine: {this.state.selectedvehicle.certification?this.state.selectedvehicle.certification['engine']:""}</p>
                                        <p style={{marginLeft:20}}>fuel: {this.state.selectedvehicle.certification?this.state.selectedvehicle.certification['fuel']:""}</p>

                                   <p>location:</p>
                                        <div style={{marginLeft:20}}>
                                             <p>type: {this.state.selectedvehicle.location?this.state.selectedvehicle.location['type']:''}</p>
                                             <p >coordinates:</p>
                                             {this.state.selectedvehicle.location? this.state.selectedvehicle.location.coordinates.map((element,i) => { return (
                                                  <p style={{marginLeft:20}} key={i}>{i}: {element}</p>
                                             )}) :""}
                                        </div>
                                   <p>tags: </p>
                                        {this.state.selectedvehicle.tags? this.state.selectedvehicle.tags.map((element,i) => { return (
                                             <p style={{marginLeft:20}} key={i}>{i}: {this.namebyid(element)}</p>
                                        )}) :""}
                                   <p>periodPrice: </p>
                                        {this.state.selectedvehicle.periodPrice? this.state.selectedvehicle.periodPrice.map((element,i) => { return (
                                             <div key={i} style={{marginLeft:20,marginTop:20}}>
                                                  <p>startDate: {this.timeConverter(element['startDate'])}</p>
                                                  <p>endDate: {this.timeConverter(element['endDate'])}</p>
                                                  <p>price: {element['price']}</p>
                                             </div>
                                        )}) :""}
                                   <p>periodPrice: </p>
                                        {this.state.selectedvehicle.photos? this.state.selectedvehicle.photos.map((element,i) => { return (
                                             <p style={{marginLeft:20}} key={i}>image: {element['image']}</p>
                                        )}) :""} 
                                   <p>isAutoNegotiation: {this.state.selectedvehicle.isAutoNegotiation===true?"true":"false"}</p>
                                   <p>bottomPrice: {this.state.selectedvehicle.bottomPrice}</p>
                                   <p>salePrice: {this.state.selectedvehicle.salePrice}</p>
                                   <p>tipe: {this.state.selectedvehicle.tipe}</p>
                                   <p>year: {this.state.selectedvehicle.year}</p>
                                   <p>transmittion: {this.state.selectedvehicle.transmittion}</p>
                                   <p>machine: {this.state.selectedvehicle.machine}</p>
                                   <p>kilometer: {this.state.selectedvehicle.kilometer}</p>
                                   <p>passenger: {this.state.selectedvehicle.passenger}</p>
                                   <p>color: {this.state.selectedvehicle.color}</p>
                                   <p>description: {this.state.selectedvehicle.description}</p>
                                   <p>brand: {this.namebyid(this.state.selectedvehicle.brand)}</p>
                                   <p>varian: {this.namebyid(this.state.selectedvehicle.varian)}</p>
                                   <p>model: {this.namebyid(this.state.selectedvehicle.model)}</p>
                                   <p>title: {this.state.selectedvehicle.title}</p>
                                   <p>user: {this.getusername(this.state.selectedvehicle.user)}</p>
                                   <p>viewCount: {this.state.selectedvehicle.viewCount}</p>
                              </Modal.Body>  
                              <Modal.Footer>
                                   <Button variant="light" className="addadmin" onClick={this.closeModal}>Close</Button>
                              </Modal.Footer>  
                                             
                         </Modal>                    
                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }