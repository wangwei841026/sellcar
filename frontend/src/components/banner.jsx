import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button,Form} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';
import DatePicker from "react-datepicker";
export default class Banner extends Component {
     constructor(props) {
          super(props);
          this.state = {
              banners: [],
              tablelist:[],
              updatebanner:[],
              deletebanner:'',
              titile:'',
              target:'',
              image_url:'',
              startDate:'',
              endDate:'',
              updatestartDate:"",
              updateendDate:"",
              addmodal: false,
              updatemodal:false,
              alertmodal:false,
          };
          this.openaddModal = this.openaddModal.bind(this);
          this.closeaddModal = this.closeaddModal.bind(this);
          this.openupdateModal = this.openupdateModal.bind(this);
          this.closeupdateModal = this.closeupdateModal.bind(this);
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);   
          this.startdateChange = this.startdateChange.bind(this);
          this.enddateChange = this.enddateChange.bind(this);
          this.updatestartdateChange = this.updatestartdateChange.bind(this);
          this.updateenddateChange = this.updateenddateChange.bind(this);
     }

     componentWillMount(){
          var self=this;
          var result = [];
          axios.get('http://localhost:4000/api/getbanners')
               .then(response => {
                    
                    this.setState({banners: response.data});
                         response.data.map(function(banner, i){
                              return result.push({"no":(i+1), "title":banner.title, 
                              "target":banner.target, 
                              "image_url":banner.image_url,
                              "startDate":self.timeConverter(banner.startDate),
                              "endDate":self.timeConverter(banner.endDate),
                              "update":<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatebanner(e, banner._id)}}>Update</MDBBtn>,
                              "remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deletebanner(e, banner._id)}}>Remove</MDBBtn>});
                                                  
                         });
                    this.setState({tablelist: result});
               
          })   
     }
     startdateChange(date) {
          this.setState({startDate: date});
        }
     enddateChange(date) {
          this.setState({endDate: date});
        }
     updatestartdateChange(date) {
          this.setState({updatestartDate: date});
     }
     updateenddateChange(date) {
          this.setState({updateendDate: date});
     }
     openaddModal() {
          this.setState({addmodal: true});
     }
       
     closeaddModal() {
          this.setState({addmodal: false});
     }
     openupdateModal() {
          this.setState({updatemodal: true});
     }
       
     closeupdateModal() {
          this.setState({updatemodal: false});
     }

     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 
     timeConverter(UNIX_timestamp) {
          var a = new Date(UNIX_timestamp);
          var year = a.getFullYear();
          var month = a.getMonth() + 1;
          month = month.toString().length === 1 ? "0" + month : month;
          var date = a.getDate();
          date = date.toString().length === 1 ? "0" + date : date;
          var time = month + '/' + date + '/' + year;
  
          return time;
      }
          

     updatebanner(event,id) {
          this.setState({updatemodal: true});
          
          var updatebanner=this.state.banners.find((e) => e._id === id);
          this.setState({updatebanner:updatebanner});
          this.setState({updatestartDate:new Date(updatebanner['startDate']) });
          this.setState({updateendDate:new Date(updatebanner['endDate']) });
          
      }
      update= (e) => {
          var self=this;
          e.preventDefault();
          var requestbanner={
               id:this.state.updatebanner["_id"],
               title:this.updatetitle.value,
               target:this.updatetarget.value,
               image_url:this.updateimage_url.value,
               startDate:this.state.updatestartDate,
               endDate:this.state.updateendDate

          }
          axios.post('http://localhost:4000/api/updatebanner', requestbanner)
              .then(res => {
                   if (res.data==="success") {
                        self.closeupdateModal();
                        window.location.reload();
                    }
              })
      }
      deletebanner(event, id) {
          this.setState({deletebanner: id});
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deletebanner/'+this.state.deletebanner)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              .catch(function (error) {
                  console.log(error);
              })
      }
      
      onChange = (e) => {
          this.setState({ [e.target.name]: e.target.value });
     
     } 
     addbanner=(e) => {
          var self=this;
          e.preventDefault();
          var requestbanner={
               title:this.state.title,
               target:this.state.target,
               image_url:this.state.image_url,
               startDate:this.state.startDate,
               endDate:this.state.endDate
          }
          
          axios.post('http://localhost:4000/api/addbanner', requestbanner)
              .then(res => {
                   if (res.data==="success") {
                        self.closeaddModal();
                        window.location.reload();
                    }
              })
     }    
      
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 100
                 },
                 {
                   label: 'Title',
                   field: 'title',
                   width: 250
                 },
                 {
                    label: 'Target',
                    field: 'target',
                    width: 250
                  },
                 {
                   label: 'Image_url',
                   field: 'image_url',
                   width: 250
                 },
                 {
                    label: 'Start Date',
                    field: 'startDate',
                    width: 250
                  },
                  {
                    label: 'End Date',
                    field: 'endDate',
                    width: 250
                  },
                  {
                    label: 'Update',
                    field: 'update',
                    width: 200
                  },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 200
                 }
               ],
               rows: this.state.tablelist
          };
          
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Banner</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         <Button color="primary" className="addadmin" onClick={this.openaddModal}>ADD</Button>

                    </div>
                    
                    <div>
                         <Modal show={this.state.addmodal} onHide={this.closeaddModal}>
                                   <Modal.Header closeButton>
                                        <Modal.Title>Add banner</Modal.Title>
                                   </Modal.Header>
                                   <Form onSubmit={this.addbanner}>
                                   <Modal.Body>
                                        <Form.Group >
                                             <Form.Label>title</Form.Label>
                                             <Form.Control type="text" placeholder="Enter title" name="title" onChange={this.onChange} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>target</Form.Label>
                                             <Form.Control type="text" placeholder="Enter target" name="target" onChange={this.onChange} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>image url</Form.Label>
                                             <Form.Control type="text" placeholder="Enter image url" name="image_url" onChange={this.onChange} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>startDate</Form.Label>
                                             <DatePicker selected={this.state.startDate} onChange={this.startdateChange} className="datepicker ml-3" />
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>end Date</Form.Label>
                                             <DatePicker selected={this.state.endDate} onChange={this.enddateChange} className="datepicker ml-3" />
                                        </Form.Group>
                                   </Modal.Body> 
                                   <Modal.Footer>
                                        <Button variant="primary" className="addadmin" type="submit">Add Banner</Button>
                                        <Button variant="light" className="addadmin" onClick={this.closeaddModal}>Close</Button>
                                        
                                   </Modal.Footer>  
                                   </Form>;                 
                              </Modal>                    
                         
                    </div>
                    <div>
                       
                         <Modal show={this.state.updatemodal} onHide={this.closeupdateModal}>
                                   <Modal.Header closeButton>
                                        <Modal.Title>Update banner</Modal.Title>
                                   </Modal.Header>
                                   <Form onSubmit={this.update}>
                                   <Modal.Body>
                                   <Form.Group >
                                             <Form.Label>title</Form.Label>
                                             <Form.Control type="text" placeholder="Enter title" name="title" ref={el => this.updatetitle = el} defaultValue={this.state.updatebanner['title']} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>target</Form.Label>
                                             <Form.Control type="text" placeholder="Enter target" name="target" ref={el => this.updatetarget = el}  defaultValue={this.state.updatebanner['target']} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>image url</Form.Label>
                                             <Form.Control type="text" placeholder="Enter image url" name="image_url" ref={el => this.updateimage_url = el} defaultValue={this.state.updatebanner['image_url']} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>startDate</Form.Label>
                                             <DatePicker  selected={this.state.updatestartDate}  onChange={this.updatestartdateChange} className="datepicker ml-3" />
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>endDate</Form.Label>
                                             <DatePicker  selected={this.state.updateendDate}  onChange={this.updateenddateChange} className="datepicker ml-3" />
                                        </Form.Group>
                                   </Modal.Body> 
                                   <Modal.Footer>
                                        <Button variant="primary" className="addadmin" type="submit">Update Category</Button>
                                        <Button variant="light" className="addadmin" onClick={this.closeupdateModal}>Close</Button>
                                        
                                   </Modal.Footer>  
                                   </Form>;                 
                              </Modal>                    
                         
                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }