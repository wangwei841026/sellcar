import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button,Form} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';

export default class Category extends Component {
     constructor(props) {
          super(props);
          this.state = {
              allcategories: [],
              categories:[],
              tablelist:[],
              updatecategory:[],
              deletecategory:'',
              name:'',
              parent:'',
              type:'',
              addmodal: false,
              updatemodal:false,
              alertmodal:false,
          };
          this.openaddModal = this.openaddModal.bind(this);
          this.closeaddModal = this.closeaddModal.bind(this);
          this.openupdateModal = this.openupdateModal.bind(this);
          this.closeupdateModal = this.closeupdateModal.bind(this);
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);   
          
     }

     componentWillMount(){
          var self=this;
          var result = [];
          axios.get('http://localhost:4000/api/getcategories')
               .then(response => {
                    
                    this.setState({categories: response.data});
                    axios.get('http://localhost:4000/api/getallcategories')
                    .then(response => {
                         this.setState({allcategories:response.data});
                         response.data.map(function(category, i){
                              return result.push({"no":(i+1), "name":category.name, 
                              "parent":self.namebyid(category.parent), 
                              "type":category.type,
                              "update":<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatecategory(e, category._id)}}>Update</MDBBtn>,
                              "remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deletecategory(e, category._id)}}>Remove</MDBBtn>});
                                                  
                         });
                         this.setState({tablelist: result});
                         
                    })
          })   
     }
     openaddModal() {
          this.setState({addmodal: true});
     }
       
     closeaddModal() {
          this.setState({addmodal: false});
     }
     openupdateModal() {
          this.setState({updatemodal: true});
     }
       
     closeupdateModal() {
          this.setState({updatemodal: false});
     }

     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 
     namebyid(id) {
          
          if (id!==null) {
               var parent=this.state.categories.find((e)=>e._id===id);
               
               if (parent!==undefined) {
                    return parent['name'];
               }       
          } else {
               return "";
          }
     }
          

     updatecategory(event,id) {
          this.setState({updatemodal: true});
          var updatecategory=this.state.allcategories.find((e) => e._id === id);
          this.setState({updatecategory:updatecategory});
          
      }
      update= (e) => {
          var self=this;
          e.preventDefault();
          var requestcategory={
               id:this.state.updatecategory["_id"],
               name:this.updatename.value,
               parent:this.updateparent.value,
               type:this.updatetype.value,
          }
          axios.post('http://localhost:4000/api/updatecategory', requestcategory)
              .then(res => {
                   if (res.data==="success") {
                        self.closeupdateModal();
                        window.location.reload();
                    }
              })
      }
      deletecategory(event, id) {
          this.setState({deletecategory: id});
          console.log(this.state.deletecategory);
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deletecategory/'+this.state.deletecategory)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              .catch(function (error) {
                  console.log(error);
              })
      }
      
      onChange = (e) => {
          this.setState({ [e.target.name]: e.target.value });
     
     } 
     addcategory=(e) => {
          var self=this;
          e.preventDefault();
          var requestcategory={
               name:this.state.name,
               parent:this.state.parent,
               type:this.state.type,
          }
          axios.post('http://localhost:4000/api/addcategory', requestcategory)
              .then(res => {
                   if (res.data==="success") {
                        self.closeaddModal();
                        window.location.reload();
                    }
              })
     }    
      
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 100
                 },
                 {
                   label: 'name',
                   field: 'name',
                   width: 250
                 },
                 {
                    label: 'parent',
                    field: 'parent',
                    width: 250
                  },
                 {
                   label: 'type',
                   field: 'type',
                   width: 250
                 },
                  {
                    label: 'Update',
                    field: 'update',
                    width: 200
                  },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 200
                 }
               ],
               rows: this.state.tablelist
          };
          
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Category</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         <Button color="primary" className="addadmin" onClick={this.openaddModal}>ADD</Button>

                    </div>
                    
                    <div>
                         <Modal show={this.state.addmodal} onHide={this.closeaddModal}>
                                   <Modal.Header closeButton>
                                        <Modal.Title>Add category</Modal.Title>
                                   </Modal.Header>
                                   <Form onSubmit={this.addcategory}>
                                   <Modal.Body>
                                        <Form.Group >
                                             <Form.Label>name</Form.Label>
                                             <Form.Control type="text" placeholder="Enter name" name="name" onChange={this.onChange} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>parent</Form.Label>
                                             <Form.Control as="select" name="parent" onChange={this.onChange}>
                                                  <option value="">none</option>
                                                  {this.state.categories.map((element,i) => { return (
                                                       <option key={i} value={element["_id"]}>{element["name"]}</option>
                                                  )})}
                                                  
                                             </Form.Control>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>type</Form.Label>
                                             <Form.Control type="text" placeholder="Enter type" name="type" onChange={this.onChange} disabled = {(this.state.parent==="")? "disabled" : ""} required/>
                                        </Form.Group>
                                   </Modal.Body> 
                                   <Modal.Footer>
                                        <Button variant="primary" className="addadmin" type="submit">Add Category</Button>
                                        <Button variant="light" className="addadmin" onClick={this.closeaddModal}>Close</Button>
                                        
                                   </Modal.Footer>  
                                   </Form>;                 
                              </Modal>                    
                         
                    </div>
                    <div>
                       
                         <Modal show={this.state.updatemodal} onHide={this.closeupdateModal}>
                                   <Modal.Header closeButton>
                                        <Modal.Title>Update category</Modal.Title>
                                   </Modal.Header>
                                   <Form onSubmit={this.update}>
                                   <Modal.Body>
                                        <Form.Group >
                                             <Form.Label>name</Form.Label>
                                             <Form.Control type="text" placeholder="Enter name" name="name" ref={el => this.updatename = el} defaultValue={this.state.updatecategory['name']}required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>parent</Form.Label>
                                             <Form.Control as="select" name="parent" ref={el => this.updateparent = el} defaultValue={this.state.updatecategory['parent']? this.state.updatecategory['parent']:"" }>
                                                  <option value="">none</option>
                                                  {this.state.categories.map((element,i) => { return (
                                                        
                                                       <option key={i} value={element["_id"]}>{element["name"]}</option>
                                                       
                                                       
                                                  )})}
                                                  
                                             </Form.Control>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>type</Form.Label>
                                             <Form.Control type="text" placeholder="Enter type" name="type" ref={el => this.updatetype = el} defaultValue={this.state.updatecategory['type']?this.state.updatecategory['type']:""}/>
                                        </Form.Group>
                                   </Modal.Body> 
                                   <Modal.Footer>
                                        <Button variant="primary" className="addadmin" type="submit">Update Category</Button>
                                        <Button variant="light" className="addadmin" onClick={this.closeupdateModal}>Close</Button>
                                        
                                   </Modal.Footer>  
                                   </Form>;                 
                              </Modal>                    
                         
                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }