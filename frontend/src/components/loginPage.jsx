import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {loginUserAction} from '../actions/authenticationActions';
import {FormErrors} from "./FormErrors";

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            formErrors: {
                email: '',
                password: ''
            },
            emailValid: false,
            passwordValid: false,
            formValid: false
        }
    }
    onHandleLogin = (event) => {
        event.preventDefault();
        let email = event.target.email.value;
        let password = event.target.password.value;

        const data = {
            email,
            password
        };
        this.props.dispatch(loginUserAction(data));
        console.log(this.props);
        console.log(this.state);
    }
    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        }, () => {
            this.validateField(name, value)
        });
    }
    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email': emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password': passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({
            formValid: this.state.emailValid && this.state.passwordValid
        });
    }
    showpass() {
        if (this.passInput.type==="password") {
            this.passInput.type="text";
        } else {
            this.passInput.type="password";
        }
    }
    render() {

        let isSuccess,
            message;

        if (this.props.response.login.hasOwnProperty('response')) {
            isSuccess = this.props.response.login.response.success;
            message = this.props.response.login.response.message;

            if (isSuccess) {
                localStorage.removeItem('token');
                localStorage.setItem('token', this.props.response.login.response.token);
            }
        }

        return (
            <div className="login-form">
                <div className="row w-100 m-0">
                    <div className="col-12 col-lg-4 ml-auto pr-0 logoimg"> 
                        <img src="./assets/img/logo.png" className="w-100" alt="logo" style={{height:550}}/>
                    </div>
                    <div className="col-12 col-lg-3 mr-auto pl-0 logincontent">
                        
                        <form className="form-signin" onSubmit={this.onHandleLogin} autoComplete="off">
                            <h1 className="h3 my-5 font-weight-normal">iDeal Admin Login</h1>
                            {!isSuccess ? <div className="errortext">{message}</div> : <Redirect push to="/dashboard"/>}
                            <div className="errortext">
                                <FormErrors formErrors={this.state.formErrors}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputEmail" className="logintxt">Email</label>
                                <input type="email" id="inputEmail" name="email" className="form-control mb-5" placeholder="Email address" required />
                            </div>
                            <div className="input-group custom input-group-lg">
                                <label htmlFor="inputPassword" className="logintxt">Password</label>
                                <input type="password" id="inputPassword" name="password" className="form-control mb-5 passinput" placeholder="Password" 
                                 ref={(input) => { this.passInput = input; }} required/>
                                <div className="input-group-append custom">
                                    <span className="input-group-text eye"><i className="fas fa-eye" aria-hidden="true" onClick={(event) => this.showpass(event)}></i></span>
                                </div>
                            </div>
                            <button className="btn loginbtn btn-primary btn-block" type="submit" >Login</button>

                            
                            <p className="copytext">iDeal &copy; 2019</p>
                        </form>
                        {/* Don't have account?
                    <Link to='register'>Register here</Link>  */}
                    </div>
                </div>
            </div>
        );
    }


}

const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(LoginPage);
