import React, {Component} from 'react';
import Header from "./header";
import Admin from "./admin"
import {Switch} from "react-router";
import {Route} from 'react-router';
import Buyer from "./buyer";
import Seller from "./seller";
import Category from "./category";
import Banner from "./banner";
import Vehicle from "./vehicle";
import Transaction from "./transaction";
import Report from "./report";
class DashboardPage extends Component {
  
    render() {
        return (
            <div>
                <header>
                    <Header/>
                </header>

                <section>
                    <Switch>
                        <Route exact path="/dashboard" component={Admin} />
                        <Route path="/dashboard/admin" component={Admin} />
                        <Route path="/dashboard/buyer" component={Buyer} />
                        <Route path="/dashboard/seller" component={Seller} />
                        <Route path="/dashboard/category" component={Category} />
                        <Route path="/dashboard/banner" component={Banner} />
                        <Route path="/dashboard/vehicle" component={Vehicle} />
                        <Route path="/dashboard/transaction" component={Transaction} />
                        <Route path="/dashboard/report" component={Report} />
                        
                    </Switch>
                </section>
            </div>
        );
    }
}

export default DashboardPage;
