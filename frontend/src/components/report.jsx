import React, {Component} from 'react';
import axios from 'axios';
import { Table, Form } from 'react-bootstrap';
import { Line } from "react-chartjs-2";
import DatePicker from "react-datepicker";
export default class Report extends Component {
     constructor(props) {
          super(props);
          this.state = {
              transactionbyvehicle: [],
              transactionbybuyer: [],
              vehicles:[],
              buyers:[],
              sellers:[],
              categories:[],
              topsellerlist:[],
              topcarlist:[],
              topbuyerlist:[],
              topcitylist:[],
              carlisttype:"model",
              citylisttype:"city",
              chartdatatype:"day",
              startdate:"",
              enddate:"",
              transactionbydate:[]
          };
          this.startdateChange = this.startdateChange.bind(this);
          this.enddateChange = this.enddateChange.bind(this);
         
     }

     componentWillMount(){
          
          axios.get('http://localhost:4000/api/transactionbyvehicle')
          .then(response => {
               this.setState({transactionbyvehicle:response.data});
               axios.get('http://localhost:4000/api/transactionbybuyer')
               .then(response => {
                    this.setState({transactionbybuyer:response.data});
                    axios.get('http://localhost:4000/api/gettransactions')
                    .then(response => {
                         this.setState({transactions:response.data});
                         axios.get('http://localhost:4000/api/getvehicles')
                         .then(response => {
                              this.setState({vehicles:response.data});
                              axios.get('http://localhost:4000/api/getallbuyers')
                              .then(response => {
                                   this.setState({buyers:response.data});
                                   axios.get('http://localhost:4000/api/getallsellers')
                                   .then(response => {
                                        this.setState({sellers:response.data});
                                        axios.get('http://localhost:4000/api/getallcategories')
                                        .then(response => {
                                             this.setState({categories:response.data});
                                             this.setState({topsellerlist:this.getsubarray(this.state.transactionbyvehicle,5)});
                                             this.setState({topcarlist:this.getsubarray(this.state.transactionbyvehicle,5)});
                                             this.setState({topbuyerlist:this.getsubarray(this.state.transactionbybuyer,5)});
                                             this.setState({topcitylist:this.getsubarray(this.state.transactionbybuyer,5)});
                                             console.log(this.state)
                                        })
                                   })
                              })
                        })    
                    })
               })     
          })
          
               
     }
     
     getsubarray(array, num) {
          var subarray=[];
          array.forEach(function(ele, i){
               if (i<num) {
                    subarray.push(ele);
               }
          });
          return subarray;
     }
      
     getsellername(id) {
          var seller=this.state.sellers.find((e)=>e._id===id);
          if (seller!==undefined) {
               return seller['firstName'];
          } else {
               return "";
          }
          // var sellerid;  
          // if (id!==undefined) {
          //      var vehilce=this.state.vehicles.find((e)=>e._id===id);
          //      if (vehilce!==undefined) {
          //           sellerid=vehilce['user'];
          //           var seller=this.state.sellers.find((e)=>e._id===sellerid);
                    
          //           if (seller!==undefined) {
          //                return seller['firstName'];
          //           }

          //      }  
          // } else {
          //      return "";
          // }
          
     }
     getbuyername(id) {
          var buyer=this.state.buyers.find((e)=>e._id===id);
          if (buyer!==undefined) {
               return buyer['firstName'];
          } else {
               return "";
          }
     }
     getcityname(id) {
          var buyer=this.state.buyers.find((e)=>e._id===id);
          if (buyer!==undefined) {
               return buyer['city'];
          } else {
               return "";
          }
     }
     getprovincename(id) {
          var buyer=this.state.buyers.find((e)=>e._id===id);
          if (buyer!==undefined) {
               return buyer['province'];
          } else {
               return "";
          }
     }
     getcategoryname(id) {
          var category=this.state.categories.find((e)=>e._id===id);
          if (category!==undefined) {
               return category['name'];
          } else {
               return "";
          }
     }
     setsellerlist=(e)=> {
          var value=e.target.value;
          this.setState({topsellerlist:this.getsubarray(this.state.transactionbyvehicle,value)});
          //this.drawsellertable();
          
     }
     setcarlist=(e)=> {
          var value=e.target.value;
          this.setState({topcarlist:this.getsubarray(this.state.transactionbyvehicle,value)});
          //this.drawsellerable();
          
     }
     setcarlisttype=(e)=> {
          var value=e.target.value;
          this.setState({carlisttype:value});
         // this.drawcartable();
          
     }
     setbuyerlist=(e)=> {
          var value=e.target.value;
          this.setState({topbuyerlist:this.getsubarray(this.state.transactionbybuyer,value)});
          //this.drawbuyertable();
          
     }
     setcitylist=(e)=> {
          var value=e.target.value;
          this.setState({topcitylist:this.getsubarray(this.state.transactionbyvehicle,value)});
          //this.drawcitytable();
          
     }
     setcitylisttype=(e)=> {
          var value=e.target.value;
          this.setState({citylisttype:value});
          //this.drawcitytable();
          
     }
     setcharttype=(e)=> {
          this.setState({chartdatatype:(e.target.value)});
     }
     startdateChange(date) {
          this.setState({startdate: date});
     }
     enddateChange(date) {
          this.setState({enddate: date});
     }
     drawsellertable() {
          return this.state.topsellerlist.map((element,i) => { return (
               <tr key={i}>
                    <td>{i+1}</td>
                    <td>{element['vehicle'][0]? this.getsellername(element['vehicle'][0]['user']):""}</td>
                    <td>{element.totalPrice}</td>
               </tr>
          )})
           
     }
     drawcartable() {
          if (this.state.carlisttype==="model") {
               return this.state.topcarlist.map((element,i) => { return (
                    <tr key={i} >
                         <td>{i+1}</td>
                         <td>{element['vehicle'][0]? this.getcategoryname(element['vehicle'][0]['model']):""}</td>
                         <td>{element.totalPrice}</td>
                    </tr>
               )})
          } else {
              
               return this.state.topcarlist.map((element,i) => { return (
                    
                    <tr key={i}>
                         <td>{i+1}</td>
                         <td>{element['vehicle'][0]? this.getcategoryname(element['vehicle'][0]['brand']):""}</td>
                         <td>{element.totalPrice}</td>
                    </tr>
               )})
          }
          
           
     }
     drawbuyertable() {
          return this.state.topbuyerlist.map((element,i) => { return (
               <tr key={i}>
                    <td>{i+1}</td>
                    <td>{this.getbuyername(element._id)}</td>
                    <td>{element.totalPrice}</td>
               </tr>
          )})
           
     }
     drawcitytable() {
          if (this.state.citylisttype==="city") {
               return this.state.topbuyerlist.map((element,i) => { return (
                    <tr key={i}>
                         <td>{i+1}</td>
                         <td>{this.getcityname(element._id)}</td>
                         <td>{element.totalPrice}</td>
                    </tr>
               )})
          } else {
               return this.state.topbuyerlist.map((element,i) => { return (
                    <tr key={i}>
                         <td>{i+1}</td>
                         <td>{this.getprovincename(element._id)}</td>
                         <td>{element.totalPrice}</td>
                    </tr>
               )})
          }
          
           
     }
     drawchart=(e)=> {
          var transaction;
          
          var sortdata=[];
          var x=[];
          var y=[];
          if (this.state.startdate!=="" && this.state.enddate!=="") {
               transaction=this.state.transactions.filter((e)=>(new Date(e.paymentDate)>=this.state.startdate)&&(new Date(e.paymentDate)<=this.state.enddate));
               if (this.state.chartdatatype==="day") {
                    transaction.forEach(function(ele){
                         var createdAt = ele.paymentDate.substring(0, 10);
                         var existing = sortdata.filter(function (v, i) {
                              return v.date === createdAt;
                         });
     
                         if (existing.length > 0) {
                              var existingIndex = sortdata.indexOf(existing[0]);
                              sortdata[existingIndex].value = sortdata[existingIndex].value + ele.finalPrice;
                         } else {
                              sortdata.push({ "date": createdAt, "value": ele.finalPrice });
                         }
                    })
                    sortdata.forEach(function(ele){
                         x.push(ele.date);
                         y.push(ele.value);
     
                    })
               } else {
                    transaction.forEach(function(ele){
                         var createdAt = ele.paymentDate.substring(0, 7);
                         var existing = sortdata.filter(function (v, i) {
                              return v.date === createdAt;
                         });
     
                         if (existing.length > 0) {
                              var existingIndex = sortdata.indexOf(existing[0]);
                              sortdata[existingIndex].value = sortdata[existingIndex].value + ele.finalPrice;
                         } else {
                              sortdata.push({ "date": createdAt, "value": ele.finalPrice });
                         }
                    })
                    sortdata.forEach(function(ele){
                         x.push(ele.date);
                         y.push(ele.value);
     
                    })
               }
               
          }
          var chartdata={
               labels: x,
               datasets: [
                 {
                    label: "Sales Growth Chart",
                    fill: false,
                    borderColor: "rgb(205, 130, 158)",
                    data: y
                 },
                 
               ]
             }
          
          return (
               <Line data={chartdata} options={{ responsive: true }} />
          )
     }
    
      
     render(){
          
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Reporting</h2>
                    </div> 
                    <div className="row" style={{marginTop:30}}>
                         <div className="col-6">
                              <div className="reportdiv p-4">
                                   <div className="d-flex align-items-center">
                                        <span className="reporttitle"><img src="/assets/img/cup.png" alt="topseller" className="mr-3"/>Top Seller</span>
                                        <span className="ml-auto">
                                             <Form.Control as="select" className="reportselect" onChange={this.setsellerlist}>
                                                  <option value="5">Top5</option>
                                                  <option value="10">Top10</option>
                                                  <option value="20">Top20</option>
                                                  <option value="50">Top50</option>
                                             </Form.Control>
                                        </span>    
                                   </div> 
                                   <div className="reporttable">
                                        <Table size="sm" >
                                             <thead className="tabletitle">
                                                  <tr>
                                                       <th>No</th>
                                                       <th>Seller Name</th>
                                                       <th>Total Revenue</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  {this.drawsellertable() }
                                             </tbody>
                                        </Table>
                                   </div>
                              </div>
                         </div>
                         <div className="col-6">
                              <div className="reportdiv p-4">
                                   <div className="d-flex align-items-center">
                                        <span className="reporttitle"><img src="/assets/img/car.png" alt="topscar" className="mr-3"/>Top Car</span>
                                        <div className="no-wrap ml-auto">
                                             <span >
                                                  <Form.Control as="select" className="reportselect" onChange={this.setcarlisttype}>
                                                       <option value="model">By Model</option>
                                                       <option value="brand">By Brand</option>
                                                  </Form.Control>
                                             </span>
                                        </div> 
                                        <div className="no-wrap">   
                                             <span >
                                                  <Form.Control as="select" className="reportselect" onChange={this.setcarlist}>
                                                       <option value="5">Top5</option>
                                                       <option value="10">Top10</option>
                                                       <option value="20">Top20</option>
                                                       <option value="50">Top50</option>
                                                  </Form.Control>
                                             </span>  
                                        </div>  
                                   </div> 
                                   <div className="reporttable">
                                        <Table size="sm" >
                                             <thead className="tabletitle">
                                                  <tr>
                                                       <th>No</th>
                                                       {this.state.carlisttype==="model"?<th>Car Model</th>:<th>Car Brand</th> }
                                                       <th>Total Sold</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  {this.drawcartable() }
                                             </tbody>
                                        </Table>
                                   </div>
                              </div>
                         </div>
                    </div> 
                    <div className="row" style={{marginTop:30}}>
                         <div className="col-6">
                              <div className="reportdiv p-4">
                                   <div className="d-flex align-items-center">
                                        <span className="reporttitle"><img src="/assets/img/cup.png" alt="topseller" className="mr-3"/>Top Buyer</span>
                                        <span className="ml-auto">
                                             <Form.Control as="select" className="reportselect" onChange={this.setbuyerlist}>
                                                  <option value="5">Top5</option>
                                                  <option value="10">Top10</option>
                                                  <option value="20">Top20</option>
                                                  <option value="50">Top50</option>
                                             </Form.Control>
                                        </span>    
                                   </div> 
                                   <div className="reporttable">
                                        <Table size="sm" >
                                             <thead className="tabletitle">
                                                  <tr>
                                                       <th>No</th>
                                                       <th>Buyer Name</th>
                                                       <th>Total Points</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  {this.drawbuyertable() }
                                             </tbody>
                                        </Table>
                                   </div>
                              </div>
                         </div>
                         <div className="col-6">
                              <div className="reportdiv p-4">
                                   <div className="d-flex align-items-center">
                                        <span className="reporttitle"><img src="/assets/img/city.png" alt="topscar" className="mr-3"/>Top City</span>
                                        <div className="no-wrap ml-auto">
                                             <span >
                                                  <Form.Control as="select" className="reportselect" onChange={this.setcitylisttype}>
                                                       <option value="city">Top City</option>
                                                       <option value="province">Top Province</option>
                                                  </Form.Control>
                                             </span>
                                        </div> 
                                        <div className="no-wrap">   
                                             <span >
                                                  <Form.Control as="select" className="reportselect" onChange={this.setcitylist}>
                                                       <option value="5">Top5</option>
                                                       <option value="10">Top10</option>
                                                       <option value="20">Top20</option>
                                                       <option value="50">Top50</option>
                                                  </Form.Control>
                                             </span>  
                                        </div>  
                                          
                                   </div> 
                                   <div className="reporttable">
                                        <Table size="sm" >
                                             <thead className="tabletitle">
                                                  <tr>
                                                       <th>No</th>
                                                       {this.state.citylisttype==="city"?<th>City Name</th>:<th>Province Name</th>}
                                                       <th>Total Sold</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  {this.drawcitytable()}
                                             </tbody>
                                        </Table>
                                   </div>
                              </div>
                         </div>
                    </div> 
                    <div className="row d-flex p-4" style={{marginTop:30}}>
                         <span className="growthtitle">Sales Growth</span>
                         <span className="ml-auto">
                              <Form.Control as="select" onChange={this.setcharttype}>
                                   <option value="day">By Day</option>
                                   <option value="month">By Month</option>
                                   
                                   
                              </Form.Control>
                         </span>
                    </div>
                    <div className="row">
                         <div className="col-12 reportdiv p-3">

                              <div className="d-flex p-3">
                                   <span>startdate: <DatePicker selected={this.state.startdate} onChange={this.startdateChange} className="ml-3" /></span>
                                   <span className="ml-5">end date: <DatePicker selected={this.state.enddate} onChange={this.enddateChange} className="ml-3" /></span>
                              </div>
                             {this.drawchart()}
                              
                         </div>
                    </div>
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }