import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';

export default class Seller extends Component {
     constructor(props) {
          super(props);
          this.state = {
              sellers: [],
              tablelist:[],
              selectedseller:[],
              deleteseller:'',
              modalIsOpen: false,
              alertmodal:false,
          };
          this.openModal = this.openModal.bind(this);
          this.closeModal = this.closeModal.bind(this);
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);      
     }

     componentWillMount(){
          var self=this;
          axios.get('http://localhost:4000/api/getallsellers')
               .then(response => {
                    this.setState({sellers:response.data});
                    var result = [];
                    var statustag;
                    response.data.map(function(seller, i){
                         
                         if (seller.status===1) {
                              statustag=<p className="activestatus">Active</p>;
                          } else if (seller.status===0)  {
                               statustag=<p>Deleted</p>;
                          } else if (seller.status===2) {
                               statustag=<p>Suspended</p>;
                          } else if (seller.status===3) {
                               statustag=<p>Pending</p>;
                          }
                         return result.push({"no":(i+1), "firstName":seller.firstName, "lastName":seller.lastName, "email":seller.email,
                         "status":statustag, "handphone":seller.handphone, "ktpId":seller.ktpId,
                         "viewdetails":<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.showseller(e, seller._id)}}>View Details</MDBBtn>,
                         "remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deleteseller(e, seller._id)}}>Remove</MDBBtn>});
                                                 
                    });
                    this.setState({tablelist: result});
                    
               })
               .catch(function (error) {
                    console.log(error);
               })
     }
     openModal() {
          this.setState({modalIsOpen: true});
     }
       
     closeModal() {
          this.setState({modalIsOpen: false});
     }

     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 

     showseller(event,id) {
          this.setState({modalIsOpen: true});
          var selectedseller=this.state.sellers.find((e) => e._id === id);
          this.setState({selectedseller:selectedseller});
      }
      deleteseller(event, id) {
          this.setState({deleteseller: id});
          console.log(this.state.deleteseller);
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deleteseller/'+this.state.deleteseller)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              .catch(function (error) {
                  console.log(error);
              })
      }
      
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 50
                 },
                 {
                   label: 'First Name',
                   field: 'firstName',
                   width: 100
                 },
                 {
                    label: 'Last Name',
                    field: 'lastName',
                    width: 100
                  },
                 {
                   label: 'Email',
                   field: 'email',
                   width: 100
                 },
                 {
                    label: 'Status',
                    field: 'status',
                    width: 100
                  },
                  {
                    label: 'Handphone',
                    field: 'handphone',
                    width: 100
                  },
                  {
                    label: 'ktpID',
                    field: 'ktpId',
                    width: 100
                  },
                  {
                    label: 'View Deails',
                    field: 'viewdetails',
                    width: 100
                  },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 100
                 }
               ],
               rows: this.state.tablelist
          };
         
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Seller</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />

                    </div>
                    
                    <div>
                         <Modal show={this.state.modalIsOpen} onHide={this.closeModal} size="lg">
                              <Modal.Header closeButton>
                                   <Modal.Title>View Details</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                   <p>firstname: {this.state.selectedseller.firstName}</p>
                                   <p>lastname: {this.state.selectedseller.lastName}</p>
                                   <p>email: {this.state.selectedseller.email}</p>
                                   <p>ktpID: {this.state.selectedseller.ktpId}</p>
                                   {this.state.selectedseller.status===1? <p>status: <span className="activestatus">Active</span></p>:<p>status: Suspended</p>}
                                   <p>status: {this.state.selectedseller.lastName}</p>
                                   <p>handphone: {this.state.selectedseller.handphone}</p>
                                   <p>otp: {this.state.selectedseller.otp}</p>
                                   <p>photo: {this.state.selectedseller.photo}</p>
                                   <p>settings: </p>
                                        {this.state.selectedseller.settings? <p style={{marginLeft:20}}>language: {this.state.selectedseller.settings['language']}</p>:""}
                                        {this.state.selectedseller.settings? <p style={{marginLeft:20}}>pushNotification: {this.state.selectedseller.settings['pushNotification']===true? "true":"false"}</p>:""}
                                   <p>survey: </p>
                                        {this.state.selectedseller.survey? this.state.selectedseller.survey.map((element,i) => { return (
                                             <div key={i} style={{marginLeft:20, marginTop:10}}>
                                                  <p >question: {element['question']}</p>
                                                  <p>answer: {element['answer']}</p>
                                             </div>)}) :""}
                              </Modal.Body>  
                              <Modal.Footer>
                                   <Button variant="light" className="addadmin" onClick={this.closeModal}>Close</Button>
                              </Modal.Footer>  
                                             
                         </Modal>                    
                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }