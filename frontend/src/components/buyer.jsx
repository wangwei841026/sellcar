import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';

export default class Buyer extends Component {
     constructor(props) {
          super(props);
          this.state = {
              buyers: [],
              tablelist:[],
              selectedbuyer:[],
              deletebuyer:'',
              modalIsOpen: false,
              alertmodal:false,
          };
          this.openModal = this.openModal.bind(this);
          this.closeModal = this.closeModal.bind(this);
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);      
     }

     componentWillMount(){
          var self=this;
          axios.get('http://localhost:4000/api/getallbuyers')
               .then(response => {
                    this.setState({buyers:response.data});
                    var result = [];
                    var statustag;
                    response.data.map(function(buyer, i){
                         
                         if (buyer.status===1) {
                             statustag=<p className="activestatus">Active</p>;
                         } else if (buyer.status===0)  {
                              statustag=<p>Deleted</p>;
                         } else if (buyer.status===2) {
                              statustag=<p>Suspended</p>;
                         } else if (buyer.status===3) {
                              statustag=<p>Pending</p>;
                         }
                         return result.push({"no":(i+1), "firstName":buyer.firstName, "lastName":buyer.lastName, "email":buyer.email,
                         "status":statustag, "handphone":buyer.handphone, "ktpId":buyer.ktpId,
                         "viewdetails":<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.showbuyer(e, buyer._id)}}>View Details</MDBBtn>,
                         "remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deletebuyer(e, buyer._id)}}>Remove</MDBBtn>});
                                                 
                    });
                    this.setState({tablelist: result});
                    
               })
               .catch(function (error) {
                    console.log(error);
               })
     }
     openModal() {
          this.setState({modalIsOpen: true});
     }
       
     closeModal() {
          this.setState({modalIsOpen: false});
     }

     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 

     showbuyer(event,id) {
          this.setState({modalIsOpen: true});
          var selectedbuyer=this.state.buyers.find((e) => e._id === id);
          this.setState({selectedbuyer:selectedbuyer});
      }
      deletebuyer(event, id) {
          this.setState({deletebuyer: id});
          console.log(this.state.deletebuyer);
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deletebuyer/'+this.state.deletebuyer)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              .catch(function (error) {
                  console.log(error);
              })
      }
      
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 100
                 },
                 {
                   label: 'First Name',
                   field: 'firstName',
                   width: 270
                 },
                 {
                    label: 'Last Name',
                    field: 'lastName',
                    width: 250
                  },
                 {
                   label: 'Email',
                   field: 'email',
                   width: 250
                 },
                 {
                    label: 'Status',
                    field: 'status',
                    width: 100
                  },
                  {
                    label: 'Handphone',
                    field: 'handphone',
                    width: 200
                  },
                  {
                    label: 'ktpID',
                    field: 'ktpId',
                    width: 250
                  },
                  {
                    label: 'View Deails',
                    field: 'viewdetails',
                    width: 200
                  },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 200
                 }
               ],
               rows: this.state.tablelist
          };
         
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Buyer</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />

                    </div>
                    
                    <div>
                         <Modal show={this.state.modalIsOpen} onHide={this.closeModal} size="lg">
                              <Modal.Header closeButton>
                                   <Modal.Title>View Details</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                   <p>firstname: {this.state.selectedbuyer.firstName}</p>
                                   <p>lastname: {this.state.selectedbuyer.lastName}</p>
                                   <p>email: {this.state.selectedbuyer.email}</p>
                                   <p>ktpID: {this.state.selectedbuyer.ktpId}</p>
                                   {this.state.selectedbuyer.status===1? <p>status: <span className="activestatus">Active</span></p>:<p>status: Suspended</p>}
                                   <p>status: {this.state.selectedbuyer.lastName}</p>
                                   <p>handphone: {this.state.selectedbuyer.handphone}</p>
                                   <p>otp: {this.state.selectedbuyer.otp}</p>
                                   <p>photo: {this.state.selectedbuyer.photo}</p>
                                   <p>settings: </p>
                                        {this.state.selectedbuyer.settings? <p style={{marginLeft:20}}>language: {this.state.selectedbuyer.settings['language']}</p>:""}
                                        {this.state.selectedbuyer.settings? <p style={{marginLeft:20}}>pushNotification: {this.state.selectedbuyer.settings['pushNotification']===true? "true":"false"}</p>:""}
                                   <p>survey: </p>
                                        {this.state.selectedbuyer.survey? this.state.selectedbuyer.survey.map((element,i) => { return (
                                             <div key={i} style={{marginLeft:20, marginTop:10}}>
                                                  <p >question: {element['question']}</p>
                                                  <p>answer: {element['answer']}</p>
                                             </div>)}) :""}
                              </Modal.Body>  
                              <Modal.Footer>
                                   <Button variant="light" className="addadmin" onClick={this.closeModal}>Close</Button>
                              </Modal.Footer>  
                                             
                         </Modal>                    
                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }