import React, {Component} from 'react';
import { Navbar,Nav,NavDropdown } from 'react-bootstrap';


export default class Header extends Component {
     
     componentWillMount () {
          
     }
     logout(event) {
          window.localStorage.removeItem('token');
          window.location.reload();
      }  
     render(){
          
          var currenturl=window.location.href;
          return (
               <div className="container">
                    <div className="row dash-header-top">
                         <div className="col-6 align-self-center">
                              <h5 className="dash-header-txt" >iDeal Super Admin</h5>
                         </div>
                         <div className="col-6 text-right align-self-center logout" onClick={(e) => {this.logout(e)}}>
                              <h5 className="dash-header-txt" >Logout <img src="/assets/img/vector.png" alt="logout" /></h5>
                              
                         </div>
                    </div>
                    <div className="row dash-header-bottom">
                         <Navbar collapseOnSelect expand="sm" bg="white" variant="light" className='nav-header'>
                              <Navbar.Toggle aria-controls="responsive-navbar-nav"  />
                              <Navbar.Collapse id="responsive-navbar-nav">
                                   <Nav className="mr-auto">
                                        <Nav.Link href="/dashboard/admin" className={currenturl.indexOf('admin')!==-1? "active mx-3":"mx-3"} >Admin</Nav.Link>
                                        <Nav.Link href="/dashboard/buyer" className={currenturl.indexOf('buyer')!==-1? "active mx-3":"mx-3"}>Buyer</Nav.Link>
                                        <Nav.Link href="/dashboard/seller" className={currenturl.indexOf('seller')!==-1? "active mx-3":"mx-3"}>Seller</Nav.Link>
                                        <NavDropdown title="Master" className={currenturl.indexOf('category')!==-1 || currenturl.indexOf('banner')!==-1? "activedropdown mx-3":"mx-3"}>
                                             <NavDropdown.Item href="/dashboard/category">Category</NavDropdown.Item>
                                             <NavDropdown.Item href="/dashboard/banner">Banner</NavDropdown.Item>
                                        </NavDropdown>
                                        <Nav.Link href="/dashboard/report" className={currenturl.indexOf('report')!==-1? "active mx-3":"mx-3"}>Reporting</Nav.Link>
                                        <Nav.Link href="/dashboard/vehicle"  className={currenturl.indexOf('vehicle')!==-1? "active mx-3":"mx-3"}>Vehicle</Nav.Link>
                                        <Nav.Link href="/dashboard/transaction" className={currenturl.indexOf('transaction')!==-1? "active mx-3":"mx-3"}>Transaction</Nav.Link>
                                   </Nav>
                                   
                              </Navbar.Collapse>
                         </Navbar>
                    </div>                    
               </div>
          );
     }
 }