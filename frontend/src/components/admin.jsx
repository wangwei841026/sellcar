import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button,Form} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';

export default class Admin extends Component {
     constructor(props) {
          super(props);
          this.state = {
              admins: [],
              selectedadmin:'',
              modalIsOpen: false,
              alertmodal:false,
              email:'',
              firstname:'',
              lastname:'',
              password:''
          };
          this.openModal = this.openModal.bind(this);
          this.closeModal = this.closeModal.bind(this);
          this.openalertModal=this.openalertModal.bind(this);
          this.closealertModal=this.closealertModal.bind(this);      
     }

     componentWillMount(){
          var self=this;
          axios.get('http://localhost:4000/api/getalladmins')
               .then(response => {
                    var result = [];
                    
                    response.data.map(function(admin, i){
                        return result.push({"no":(i+1), "firstName":admin.firstName, "lastName":admin.lastName, "email":admin.email,"remove":<MDBBtn color="red" className="removebtn"outline size="sm" onClick={(e) => {self.deleteadmin(e, admin._id)}}>Remove</MDBBtn>});
                                                 
                    });
                    this.setState({admins: result});
                    
               })
               .catch(function (error) {
                    console.log(error);
               })
     }

     openModal() {
          this.setState({modalIsOpen: true});
     }
       
     closeModal() {
          this.setState({modalIsOpen: false});
     }
     openalertModal() {
          
          this.setState({alertmodal: true});
     }
       
     closealertModal() {
          this.setState({alertmodal: false});
     } 
      deleteadmin(event, id) {
          this.setState({selectedadmin: id});
          this.openalertModal();
      } 
      delete = (e) => {
           axios.get('http://localhost:4000/api/deleteadmin/'+this.state.selectedadmin)
              .then(response => {
                  if (response.data==="success") {
                      window.location.reload();
                  }
                  
              })
              .catch(function (error) {
                  console.log(error);
              })
      }
      onChange = (e) => {
          
          this.setState({ [e.target.name]: e.target.value });
     
     }     
      addadmin=(e) => {
           var self=this;
           e.preventDefault();
           var requestadmin={
                email:this.state.email,
                firstname:this.state.firstname,
                lastname:this.state.lastname,
                password:this.state.password

           }
           axios.post('http://localhost:4000/api/addadmin', requestadmin)
               .then(res => {
                    if (res.data==="success") {
                         self.closeModal();
                         window.location.reload();
                     }
               })
      }
     
     render(){
          const data = {
               columns: [
                 {
                   label: 'No',
                   field: 'no',
                   width: 100
                 },
                 {
                   label: 'First Name',
                   field: 'firstname',
                   width: 270
                 },
                 {
                    label: 'Last Name',
                    field: 'lastname',
                    width: 270
                  },
                 {
                   label: 'Email',
                   field: 'email',
                   width: 300
                 },
                 {
                   label: 'Remove',
                   field: 'remove',
                   width: 100
                 }
               ],
               rows: this.state.admins
          };
          return (
               <div className="container content">
                   
                    <div className="row">
                         <h2 className="content-title mt-0">Admin</h2>
                    </div> 
                    <div className="contenttable">
                         <MDBDataTable
                              sortable={false}
                              responsive={true}
                              paging={false}
                              data={data}
                         />

                         <Button color="primary" className="addadmin" onClick={this.openModal}>ADD</Button>
                    </div>
                    
                    <div>
                         <Modal show={this.state.modalIsOpen} onHide={this.closeModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Add admin</Modal.Title>
                              </Modal.Header>
                              <Form onSubmit={this.addadmin}>
                              <Modal.Body>
                                        <Form.Group >
                                             <Form.Label>Email address</Form.Label>
                                             <Form.Control type="email" placeholder="Enter email" name="email" onChange={this.onChange} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>First Name</Form.Label>
                                             <Form.Control type="text" placeholder="Enter first name"  name="firstname" onChange={this.onChange} required/>
                                        </Form.Group>
                                        <Form.Group>
                                             <Form.Label>Last Name</Form.Label>
                                             <Form.Control type="text" placeholder="Enter last name" name="lastname" onChange={this.onChange}  required/>
                                        </Form.Group>
                                        <Form.Group >
                                             <Form.Label>Password</Form.Label>
                                             <Form.Control type="password" placeholder="Enter password" name="password"  onChange={this.onChange} required/>
                                        </Form.Group>
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" type="submit">Add Admin</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closeModal}>Close</Button>
                                   
                              </Modal.Footer>  
                              </Form>;                 
                         </Modal>                    
                    </div>
                    <div>
                         <Modal show={this.state.alertmodal} onHide={this.closealertModal}>
                              <Modal.Header closeButton>
                                   <Modal.Title>Warning...</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                    <p className="alerttxt">Are you sure?</p>    
                              </Modal.Body> 
                              <Modal.Footer>
                                   <Button variant="primary" className="addadmin" onClick={this.delete} >OK</Button>
                                   <Button variant="light" className="addadmin" onClick={this.closealertModal}>Cancel</Button>
                              </Modal.Footer>  
                         </Modal>                    
                    </div>
               
               
                    <p className="footertext">iDeal &copy; 2019</p>
                             
               </div>
               
          );
     }
 }


                    