import React from 'react';
import {Route} from 'react-router';
import LoginPage from '../components/loginPage';
import RegisterPage from '../components/registerPage';
import DashboardPage from '../components/dashboardPage';
import {Router, Redirect} from "react-router-dom";
import history from '../history';

function isLoggedIn() {
    if (localStorage.getItem('token')) {
        return true;
    }

    return false;
}



export default (
    <Router history={history}>
        <Route exact path="/"  component={LoginPage}/>
        <Route path='/login'  component={LoginPage}/>
        <Route path='/register'  component={RegisterPage}/>
        <Route path='/dashboard'  render={() => (
            !isLoggedIn() ? (<Redirect to="/login" push/>) : (<DashboardPage/>))}/>
    </Router>
);
